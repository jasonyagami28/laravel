<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@home');

Route::get('/form', 'AuthController@form');


Route::get('/welcome_page', 'AuthController@welcome_sanbercode');
Route::post('/welcome_page', 'AuthController@welcome_sanbercode_post');

Route::get('/tes/{tester}', function($tester){
    return "cuma halaman tester $tester";
});

Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});

Route::get('train/{angka}', function ($angka) {
    return view('train', ["angka"=>$angka]);
});